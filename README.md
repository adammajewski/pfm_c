# Story


I have found great images of Julia set for  c = 0.355534 - 0.337292i ( c value courtesy of Chris Thomasson):
* [Paul Bourke](http://paulbourke.net/fractals/juliaset/0.355534_m0.337292.jpg) 
* [Chris Thomasson](https://www.facebook.com/photo.php?fbid=111908386634835&set=a.110008616824812&type=3&theater)

So I tried to do similar. Here is [my version](https://commons.wikimedia.org/wiki/File:Julia%20set%20for%20f%28z%29%20%3D%20z%5E2%2B0.355534%20-0.337292%2Ai.png)  

Not so good. So what next ? I should change my algorithm 







# Results 


Screen shot of pfm file in the darktable:  
![](./images/screen_shot.png )


Result of conversion pfm file to png ( made with Image Magic , but the result of conversion using darktable is the same )
![](./images/5.png)


[Questions:](https://stackoverflow.com/questions/59228081/how-to-convert-pfm-to-png)
* How can I do conversion without loosing color gradient ?
* Are the ImageMagic / darktable conversion algorithms wrong?


## Conversion

ImageMagic ver 6

```
convert 10001.pfm -resize 500x500 5.png
```

ImageMagic ver 7

```
magic 10001.pfm 10001.png
```

```
identify -verbose 10001.pfm
Image: 10001.pfm
  Format: PFM (Portable float format)
  Class: DirectClass
  Geometry: 10000x10000+0+0
  Units: Undefined
  Colorspace: sRGB
  Type: TrueColor
  Endianess: LSB
  Depth: 32/16-bit
  Channel depth:
    Red: 16-bit
    Green: 16-bit
    Blue: 16-bit
  Channel statistics:
    Pixels: 100000000
    Red:
      min: 1.17549e-38  (1.79369e-43)
      max: 65535 (1)
      mean: 10750.5 (0.164042)
      standard deviation: 24268.5 (0.370313)
      kurtosis: 1.29224
      skewness: 1.81445
      entropy: 0.643891
    Green:
      min: 1.17549e-38  (1.79369e-43)
      max: 2.92418e+07 (446.201)
      mean: 4.66404e+06 (71.1686)
      standard deviation: 5.9158e+06 (90.2693)
      kurtosis: 1.06648
      skewness: 1.34563
      entropy: 0.121853
    Blue:
      min: 1.17549e-38  (1.79369e-43)
      max: 65535 (1)
      mean: 10750.5 (0.164042)
      standard deviation: 24268.5 (0.370313)
      kurtosis: 1.29224
      skewness: 1.81445
      entropy: 0.643891
  Image statistics:
    Overall:
      min: 1.17549e-38  (1.79369e-43)
      max: 2.92418e+07 (446.201)
      mean: 1.56185e+06 (23.8322)
      standard deviation: 1.98811e+06 (30.3366)
      kurtosis: 9.96722
      skewness: 3.12302
      entropy: 0.469879
  Rendering intent: Perceptual
  Gamma: 0.454545
  Chromaticity:
    red primary: (0.64,0.33)
    green primary: (0.3,0.6)
    blue primary: (0.15,0.06)
    white point: (0.3127,0.329)
  Matte color: grey74
  Background color: white
  Border color: srgb(223,223,223)
  Transparent color: none
  Interlace: None
  Intensity: Undefined
  Compose: Over
  Page geometry: 10000x10000+0+0
  Dispose: Undefined
  Iterations: 0
  Compression: Undefined
  Orientation: Undefined
  Properties:
    date:create: 2019-12-07T01:11:28+00:00
    date:modify: 2019-12-07T01:11:28+00:00
    signature: 9634b848ea92d33a27b237d20b46e21a6d5bf090dadbe70e82a579423bfe014e
  Artifacts:
    verbose: true
  Tainted: False
  Filesize: 1.11759GiB
  Number pixels: 100000000
  Pixels per second: 56.091MP
  User time: 1.770u
  Elapsed time: 0:02.782
  Version: ImageMagick 7.0.9-9 Q16 x86_64 2019-12-08 https://imagemagick.org
```





# Color Management System = CMS
* [Linux_color_management](https://en.wikipedia.org/wiki/Linux_color_management)
* [color in Ubuntu](https://help.ubuntu.com/stable/ubuntu-help/color.html.en)
  * [colord =  a system activated daemon](https://www.freedesktop.org/software/colord/index.html)
  * [argyll cms](https://www.argyllcms.com/)


# Gamut

My monitor [gamut](https://en.wikipedia.org/wiki/Gamut): 
![Gamut_DELL_U2713HM.png](./images/Gamut_DELL_U2713HM.png)

Tools to check gamut:
* online
  * [LCD monitor test images by Han-Kwang Nienhuys, 2008](http://www.lagom.nl/lcd-test/)
* offline
  * [displaycal](https://displaycal.net/)
  * [viewgam](https://manpages.ubuntu.com/manpages/trusty/en/man1/viewgam.1.html)

```
displaycal-profile-info
```

# How to make quality non-photo images?

## [commons tips](https://commons.wikimedia.org/wiki/Commons:Image_guidelines)
* [calibrate your monitor](https://en.wikipedia.org/wiki/Color_calibration)
* [Color space]() - usually sRGB
* [Exif](https://commons.wikimedia.org/wiki/Commons:Exif)

## Tips for making better images by [Paul Bourke](http://paulbourke.net/)
* do the fractal creation not in 8 bit, but use 16 bit or floating  point for each pixel  
* apply antialiasing by [supersampling](https://en.wikipedia.org/wiki/Supersampling)  each final pixel ( render a 30,000 pixel version of the image )
* do all [colour](http://paulbourke.net/miscellaneous/colourspace/) external to the fractal creation using gradient maps...allows you to make appearance decisions independent to the creation


# Dynamic range
* [wikipedia](https://en.wikipedia.org/wiki/Dynamic_range)


# Bit depth of data storage
* [bit depth ](https://www.camerastuffreview.com/en/what-is-the-dynamic-range-of-a-raw-file/)


[storing an image in a 32-bit HDR format is a necessary condition for an HDR image but not a sufficient one](https://www.hdrsoft.com/resources/dri.html)

Image file formats according to its possible dynamic range:
* 8-bit images (i.e. 24 bits per pixel for a color image)  = low dynamic range (JPEG, JPEG2000, PNG, TIFF, PDF)
* 16-bit (i.e. 48 bits per pixel for a color image),  example: PPM, TIFF
* 32-bit images (i.e. 96 bits per pixel for a color image) = linear high dynamic range. [Example formats](http://www.anyhere.com/gward/hdrenc/hdr_encodings.html) : PFM, EXR

## pfm
* [pfm in darktable c src code](https://github.com/darktable-org/darktable/blob/master/src/imageio/format/pfm.c)





# Contributors

are wellcome 


  
# License

A short snippet describing the license (MIT, Apache, etc.)


# technical notes
GitLab uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)
* [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [online url encoding](https://www.urlencoder.org/) of links for the markdown, in case that your  
* [CommonMark Spec](https://spec.commonmark.org/)

## API Reference

simple one file c programs which 
- do not need any extra libraries 
- can be run from console
- compiled with gcc 
- multiplatform

How to compile and run is described in the comments of c files


## Git

```git
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/pfm_c.git
git add .
git commit -m "Initial commit"
git push -u origin master
```


```git
git clone git@gitlab.com:adammajewski/pfm_c.git
```

local repo: ~/c/julia/demj/85/pfm/pfm1/test2 



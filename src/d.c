/*

https://www.facebook.com/photo.php?fbid=111908386634835&set=a.110008616824812&type=3&theater

center of period 85 componnet. Adress 1->5 -> 85
4K antialiased version. Max iterations 5000, and it was reached!

Paul Bourke I rendered a 30,000 pixel version

  Adam Majewski
  adammaj1 aaattt o2 dot pl  // o like oxygen not 0 like zero 
  
  
  console program in c programing language 
  
  ==============================================
  
  
  Structure of a program or how to analyze the program 
  
  
  ============== Image X ========================
  
  DrawImageOfX -> DrawPointOfX -> ComputeColorOfX -> IsInTheYTrap
  
  first 2 functions are identical for every X
  check only last function =  ComputeColorOfX
  which computes color of one pixel !
  
  
Vec3f(color[2], color[1], color[0]);
   
  ==========================================

  
  ---------------------------------
  indent d.c 
  default is gnu style 
  -------------------



  c console progam 
  
	export  OMP_DISPLAY_ENV="TRUE"	
  	gcc d.c -lm -Wall -march=native -fopenmp
  	time ./a.out > b.txt


  gcc d.c -lm -Wall -march=native -fopenmp


  time ./a.out

  time ./a.out >a.txt

  ----------------------
 ls -l *

=====
setup start
 end of setup 
File 10001.0000000000.dat saved 
File 10001.pfm saved 
 allways free memory (deallocate )  to avoid memory leaks 
Numerical approximation of Julia set for fc(z)= z^2 + c 
parameter c = ( 0.3555340000000000 ; -0.3372920000000000 ) 
Image Width = 2.400000 in world coordinate
PixelWidth = 0.000240 
distanceMax = 0.0002400240024002
distanceMaxImage = 2.1633996209160000
iSize = number of points in the dData array = 100000000
iSizeF = number of points in the fRGB array = 300000000
 iExterior = 29235714 	 iBoundary = 4780608 	 iInterior = 7502351 	 iSum = 41518673
iAll = 32521082 
Error : iSum != iSize 
estimated pfm file size = 1200000016 B
 Maximal number of iterations = iterMax = 50000 
ratio of image  = 1.000000 ; it should be 1.000 ...
gcc version: 7.4.0

  

*/

#include <stdio.h>
#include <stdlib.h>		// malloc
#include <string.h>		// strcat
#include <math.h>		// M_PI; needs -lm also
#include <complex.h>
#include <omp.h>	// OpenMP

/* --------------------------------- global variables and consts ------------------------------------------------------------ */



// virtual 2D array and integer ( screen) coordinate
// Indexes of array starts from 0 not 1 
//unsigned int ix, iy; // var
static unsigned int ixMin = 0;	// Indexes of array starts from 0 not 1
static unsigned int ixMax;	//
static unsigned int iWidth;	// horizontal dimension of array

static unsigned int iyMin = 0;	// Indexes of array starts from 0 not 1
static unsigned int iyMax;	//

static unsigned int iHeight = 1000;	//  
// The size of array has to be a positive constant integer 
static unsigned int iSize;	// = iWidth*iHeight; 
static unsigned int iSizeF;	// = iWidth*iHeight; 

int iColorSize = 3 ; // RGB = 3*float
// memmory 1D array 
double *dData; // double for dem ( double = 8 bytes per pixel)
float *fRGB; // float RGB ( 3 x 4 bytes = 12 bytes per pixel ) for color pfm

// unsigned int i; // var = index of 1D array
//static unsigned int iMin = 0; // Indexes of array starts from 0 not 1
static unsigned int iMax;	// = i2Dsize-1  = 
// The size of array has to be a positive constant integer 
// unsigned int i1Dsize ; // = i2Dsize  = (iMax -iMin + 1) =  ;  1D array with the same size as 2D array


static const double ZxMin = -1.2;	//-0.05;
static const double ZxMax =  1.2;	//0.75;
static const double ZyMin = -1.2;	//-0.1;
static const double ZyMax =  1.2;	//0.7;
static double PixelWidth;	// =(ZxMax-ZxMin)/ixMax;
static double PixelHeight;	// =(ZyMax-ZyMin)/iyMax;
static double ratio;


// complex numbers of parametr plane 
double complex c;		// parameter of function fc(z)=z^2 + c


static unsigned long int iterMax = 50000;	//iHeight*100;

static double ER = 160;		// EscapeRadius for bailout test  , minimal =  2.0




double distanceMax;
double distanceMaxImage = 0.0;

//double D2MaxGlobal;	//= 0.0497920256372717 ;
//double DistanceMaxGlobal2  ;


// sum of points = iSize;
int iExterior = 0;
int iInterior = 0;
int iBoundary = 0;
int iAll =0;
double PixelWidthRatio = 1.0; //  BoundaryWidth= PixelWidthRatio*PixelWidth; // here boundary is changing with resolution, maybe % of ImageWidth would be better ? 



/* ------------------------------------------ functions -------------------------------------------------------------*/





//------------------complex numbers -----------------------------------------------------



// fast cabs
double cabs2(complex double z) {
  return (creal(z) * creal(z) + cimag(z) * cimag(z));
}



// from screen to world coordinate ; linear mapping
// uses global cons
double
GiveZx ( int ix)
{
  return (ZxMin + ix * PixelWidth);
}

// uses globaal cons
double GiveZy (int iy) {
  return (ZyMax - iy * PixelHeight);
}				// reverse y axis


complex double GiveZ( int ix, int iy){
  double Zx = GiveZx(ix);
  double Zy = GiveZy(iy);
	
  return Zx + Zy*I;
	
	


}




// ****************** DYNAMICS = trap tests ( target sets) ****************************



// bailout test
// z escapes when 
// abs(z)> ER or cabs2(z)> ER2 
// https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/Julia_set#Boolean_Escape_time

int Escapes(complex double z){
 // here target set (trap) is the exterior  circle with radsius = ER ( EscapeRadius) 
  // with ceter = origin z= 0
  // on the Riemann sphere it is a circle with point at infinity as a center  
   
  if (cabs(z)>ER) return 1;
  return 0;
}








/* -----------  array functions = drawing -------------- */

/* gives position of 2D point (ix,iy) in 1D array  ; uses also global variable iWidth */
unsigned int Give_i (unsigned int ix, unsigned int iy)
{
  return ix + iy * iWidth;
}





// ***************************************************************************************************************************
// ************************** DEM/J*****************************************
// ****************************************************************************************************************************

double ComputeDistance(complex double z){
// https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/Julia_set#DEM.2FJ


  int nMax = iterMax;
  complex double dz = 1.0; //  is first derivative with respect to z.
  double distance;
  double cabsz;
	
  int n;

  // compute distance from point z to the boundary of Julia set
  for (n=0; n < nMax; n++){ //forward iteration

    if (cabs(z)> ER || cabs(dz)> 1e60) break; // big values
    
  			
    dz = 2.0*z * dz; 
    z = z*z +c ; /* forward iteration : complex quadratic polynomial */ 
  }
  
  if (n==nMax) 
  	{distance = -1.0;} // 
  	else {
  		cabsz = cabs(z);
  		distance = 2.0 * cabsz* log(cabsz)/ cabs(dz);
  		}
  // find max distance of the image 
  //if (distance > distanceMax) {distanceMax = distance;} 		
  		
   
  return distance;
}



// compute and save  raster point (ix,iy) data
int ComputePointData (double A[], int ix, int iy)
{
  int i;			/* index of 1D array */
  double distance;
  complex double z;


  i = Give_i (ix, iy);		/* compute index of 1D array from indices of 2D array */
  z = GiveZ(ix,iy);
  distance = ComputeDistance(z);
  
  if (distance >distanceMaxImage ) distanceMaxImage = distance; // compute max distance of the image
  A[i] = distance ;		// interior
  
  return 0;
}




// fill array 
// uses global var :  ...
// scanning complex plane 
int FillDataArray (double A[])
{
  int ix, iy;		// pixel coordinate 

  	//printf("compute image \n");
 	// for all pixels of image 
	#pragma omp parallel for schedule(dynamic) private(ix,iy) shared(A, ixMax , iyMax)
  	for (iy = iyMin; iy <= iyMax; ++iy){
    		printf (" %d from %d \r", iy, iyMax);	//info 
    		for (ix = ixMin; ix <= ixMax; ++ix)
      			ComputePointData(A, ix, iy);	//  
  }

  return 0;
}










int SaveDataFile(double A[], double k, char* comment ){

	 FILE * fp;
  
  	char name [100]; /* name of file */
  	snprintf(name, sizeof name, "%.10f", k); /*  */
  	char *filename =strncat(name,".dat", 4);
  
  
  
  	// save image to the pgm file 
  	fp= fopen(filename,"wb"); // create new file,give it a name and open it in binary mode 
  	fwrite(A,iSize,1,fp);  // write array with image data bytes to the file in one step 
  	fclose(fp); 
  
  	// info 
  	printf("File %s saved ", filename);
  	if (comment == NULL || strlen(comment) ==0)  
    	printf("\n");
  	else printf (". Comment = %s \n", comment); 

  	return 0;

}






// *******************************************************************************************
// **********************************  pfm file ****************************
// *********************************************************************************************



/*

input :
* int i
* array D of double numbers ( distance)

output : array of rgb colors 

*/
int ComputePointColor(const int i, const double D[], float F[], double distanceMax){


	double distance  = D[i]; // read distance form
	int iF = i*iColorSize; // compute index of F array
	// color channels from 0.0f to 1.0f  
	float R;
	float G;
	float B;
	
	double dPosition = 0.0; // distance/distanceMax;
	float fPosition = 0.0f; //(float) dPosition;
	
	if (distance<0.0)
		{fPosition = 1.0f;
		  R = 1.0f;
		  G = 1.0f;
		  B = 1.0f; 
		  iInterior +=1;
		} // interior
		else {
			dPosition =  distance/distanceMax;
			fPosition = (float) dPosition;
	
			if ( fPosition > 1.0f) // exterior 
				{	fPosition = fPosition/ 20.2f;
					R = 0.0f;  // red
					G = fPosition; // green
					B = 0.0f; // blue 
					iExterior +=1;
				} 
				else {	// boundary
					fPosition = 0.0f; 
					R = fPosition;
					G = fPosition;
					B = fPosition;
					iBoundary +=1;
					} // boundary
		} // if (fpclassify(distance) == FP_ZERO))
		
		
	//printf("fPosition = %f\n", fPosition); // info 
	//
	F[iF] 	= R;
	F[iF+1]	= G;
	F[iF+2] = B;
	
	iAll +=1;
	
	
	
	
	
	
	
	return 0;

};







// fill array f using data from d array
// uses global var :  ...
int FillColorArray (double D[], float F[])
{
  int i=0;		// array index 

  	//printf("compute image \n");
 	// for all pixels of image 
	#pragma omp parallel for schedule(dynamic) private(i) shared(iAll, iBoundary, iExterior, iInterior,  D, F, iSize)
  	for (i = 0; i < iSize; ++i){
    		//printf (" %d from %d \n", i, iSize);	//info 
    		ComputePointColor(i, D, F, distanceMax);	//  
  }
  
  //printf(" i = %d\n", i);
	
  return 0;
}














int Save_fRGB_Array_2_PFM( float F[], double k, char* comment )
{
  
  FILE * fp;
  
  char name [100]; /* name of file */
  snprintf(name, sizeof name, "%.0f", k); /*  */
  char *filename =strncat(name,".pfm", 4);
  
  
  
  // save image to the pgm file 
  fp= fopen(filename,"wb"); // create new file,give it a name and open it in binary mode 
  fprintf(fp,"PF\n%d %d\n%.1f\n",  iWidth, iHeight, -1.0f);  // write header to the file
  fwrite(F, sizeof(float), iSizeF, fp);  // write array with image data bytes to the file in one step 
  fclose(fp); 
  
  // info 
  printf("File %s saved ", filename);
  if (comment == NULL || strlen(comment) ==0)  
    printf("\n");
  else printf (". Comment = %s \n", comment); 

  return 0;
}




int info ()
{

  	int iSum =  iExterior+iBoundary+iInterior;
  	
	// display info messages
  	printf ("Numerical approximation of Julia set for fc(z)= z^2 + c \n");
  	//printf ("iPeriodParent = %d \n", iPeriodParent);
  	//printf ("iPeriodOfChild  = %d \n", iPeriodChild);
  	printf ("parameter c = ( %.16f ; %.16f ) \n", creal(c), cimag(c));
  
  	printf ("Image Width = %f in world coordinate\n", ZxMax - ZxMin);
  	printf ("PixelWidth = %f \n", PixelWidth);
  
  	if ( distanceMax<0.0 || distanceMax > ER ) printf("bad distanceMax\n");
	printf("distanceMax = %.16f\n",  distanceMax); 
	
  	printf("distanceMaxImage = %.16f\n",  distanceMaxImage); 	
	
		
  	printf("iSize = number of points in the dData array = %d\n", iSize );
  	//
  	printf("iSizeF = number of points in the fRGB array = %d\n", iSizeF);
  	printf(" iExterior = %d \t iBoundary = %d \t iInterior = %d \t iSum = %d\n", iExterior, iBoundary,iInterior, iSum);
  	printf("iAll = %d \n", iAll);
  	if (iSum != iSize) { printf("Error : iSum != iSize \n");}
  
  	
  	printf("estimated pfm file size = %d B\n ", iSizeF*((int) sizeof(float)) +16); // header ( 16 bytes ) +  series of three 4-byte IEEE 754 single precision floating point numbers for each pixel
  
  // image corners in world coordinate
  // center and radius
  // center and zoom
  // GradientRepetition
  	printf ("Maximal number of iterations = iterMax = %ld \n", iterMax);
  	printf ("ratio of image  = %f ; it should be 1.000 ...\n", ratio);
  
  
  //
  	printf("gcc version: %d.%d.%d\n",__GNUC__,__GNUC_MINOR__,__GNUC_PATCHLEVEL__); // https://stackoverflow.com/questions/20389193/how-do-i-check-my-gcc-c-compiler-version-for-my-eclipse
  	// OpenMP version is diplayed in the console 
  	return 0;
}


// *****************************************************************************
//;;;;;;;;;;;;;;;;;;;;;;  setup ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
// **************************************************************************************

int setup ()
{

  printf ("setup start\n");
  c = 0.355534-I*0.337292; //
  
  
  
  
  
	
  /* 2D array ranges */
  
  iWidth = iHeight;// square image
  iSize = iWidth * iHeight;	// size = number of points in the dData array 
  iSizeF = iSize * iColorSize ; // size of the fRGB array
  
  
  // iy
  iyMax = iHeight - 1;		// Indexes of array starts from 0 not 1 so the highest elements of an array is = array_name[size-1].
  //ix

  ixMax = iWidth - 1;

  /* 1D array ranges */
  // i1Dsize = i2Dsize; // 1D array with the same size as 2D array
  iMax = iSize - 1;		// Indexes of array starts from 0 not 1 so the highest elements of an array is = array_name[size-1].

  /* Pixel sizes */
  PixelWidth = (ZxMax - ZxMin) / ixMax;	//  ixMax = (iWidth-1)  step between pixels in world coordinate 
  PixelHeight = (ZyMax - ZyMin) / iyMax;
  ratio = ((ZxMax - ZxMin) / (ZyMax - ZyMin)) / ((float) iWidth / (float) iHeight);	// it should be 1.000 ...
	
   
	
  
  //ER2 = ER * ER; // for numerical optimisation in iteration
  
  
   	
  /* create dynamic 1D arrays for data  ( dData) and  colors ( fRGB ) */
  dData = malloc (iSize * sizeof (double));
  fRGB = malloc (iSizeF * sizeof (float));
  	
  if (dData == NULL || fRGB == NULL){
    fprintf (stderr, " Could not allocate memory");
    return 1;
  }

  
 	
  
  
  distanceMax = PixelWidthRatio*PixelWidth; // here boundary is changing with resolution, maybe % of ImageWidth would be better ? 
  
  
  
  printf (" end of setup \n");
	
  return 0;

} // ;;;;;;;;;;;;;;;;;;;;;;;;; end of the setup ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




int end(){


  printf (" allways free memory (deallocate )  to avoid memory leaks \n"); // https://en.wikipedia.org/wiki/C_dynamic_memory_allocation
  free (dData);
  free(fRGB);
  info ();
  return 0;

}

// ********************************************************************************************************************
/* -----------------------------------------  main   -------------------------------------------------------------*/
// ********************************************************************************************************************

int main () {
  setup ();
  // ******************************** DEM/J **********************************************************
  FillDataArray(dData);
  
  SaveDataFile(dData, iHeight+PixelWidthRatio, "") ; 
  FillColorArray(dData, fRGB);
  Save_fRGB_Array_2_PFM(fRGB, iHeight+PixelWidthRatio, "");
  
  end();

  return 0;
}
